# Girapha

*Girapha* is a tool for visualizing (embedding) graphs (i.e. vertices & edges).
Also, it can show how does the embedding algorithm work step-by-step.

## Current features
* **Fruchterman-Reingolds algorithm** - one of several force-directed graph drawing algorithms
* **Laying vertices on circle or positioning them at random**
* **Random coloring of the graph**
* **Export of graph to [TikZ](https://en.wikipedia.org/wiki/PGF/TikZ)**
* **Import of graph in a subset of  [DOT format](https://en.wikipedia.org/wiki/DOT_(graph_description_language))**
* **API for generating basic/interesting graphs**
  * "n-cubus" (```N x N x N``` cube)
  * complete graph ```K_n```
* **Creating random graph**
* **Online selection of algorithm parameters**

## Possible upcoming features
* applying repulsive force from window boundaries
* applying attractive force to the center of the window screen
* nice drawing of (binary) trees
* other graph drawing algorithms
  * would be great to minimize edge crossing
* stopping the algorithm when the graph is visualized and just oscillates

## Installation
Download [jar file](https://gitlab.com/sdvorak/girapha/-/releases) from GitLab.

Then run:

```java -jar Girapha-1.0.jar```