package window;

import draw.common.Renderable;
import draw.graph.algorithm.FruchtermanReingolds;
import draw.graph.representation.GraphEmbedding;
import draw.graph.representation.VertexEmbedding;
import draw.util.DotFileReader;
import draw.util.GraphGallery;
import draw.util.Vector2D;
import export.LatexTikZ;

import javax.swing.*;
import javax.swing.Timer;
import java.awt.*;
import java.awt.event.*;
import java.io.File;

import static draw.DrawingConstants.PANEL_HEIGHT;
import static draw.DrawingConstants.PANEL_WIDTH;

/**
 * Representation of GUI.
 */
public class Window implements ActionListener, MouseMotionListener, MouseListener {
    public static VertexEmbedding selectedVertex;
    private final Renderable r;

    private GraphEmbedding ge;
    private FruchtermanReingolds fr;

    public String stopButtonName = "Start";
    public JButton stopButton;
    public JButton newRandomGraphButton;
    public JButton exportToTikzButton;
    public JButton randomColorsButton;
    public JButton layVerticesOnCircleButton;
    public JButton selectGraphFromFileButton;
    public JSlider edgeLengthSlider, temperatureSlider;

    private final Timer timer;

    /**
     * Constructor.
     * @param   r   Whatever that extends <code>Renderable</code>.
     */
    public Window(Renderable r) {
        this.r = r;

        timer = new Timer(15, this);

        r.addRenderable(g -> {
            g.setColor(Color.GRAY);
            g.fillRect(0, 0, PANEL_WIDTH, PANEL_HEIGHT);
            g.setColor(Color.BLACK);
        });

        /////////////// SELECTION OF GRAPH - START ////////////

        /*
         * You can play with these types of graphs and many more
         */

        /* 1. Random graph -> usually almost planar */
        // ge = GraphGallery.getRandomGraph(10, .7);

        /* 2. complete graph on 6 vertices */
        // ge = GraphGallery.getCompleteGraph(6);

        /*
        // Saving the graph to DOT file:
        GraphToDotFile gtdf = new GraphToDotFile(ge);

        try {
            gtdf.createDotFile("/home/simon/IdeaProjects/Girapha/src/data/complete-graph-9.dot");
        } catch (Exception e) {
            e.printStackTrace();
        }
        */

        /* 3. cubus with width = 2 */
        // ge = GraphGallery.getCubus(2);

        /* 4. cubus with width = 3 */
        ge = GraphGallery.getCubus(3);

        // ge = GraphGallery.getCubus(4);

        /* 5. load some graph in a DOT format (at least subset of the format) */
        // DotFileReader dfr = new DotFileReader("src/data/somegraph001.dot");
        // ge = dfr.getGraph();

        /* 6. planar graph */
//        DotFileReader dfr = new DotFileReader("src/data/planargraph001.dot");
//        ge = dfr.getGraph();

        ge.randomColors();

        /////////////// SELECTION OF GRAPH - END ////////////

        fr = new FruchtermanReingolds(ge);

        stopButton = new JButton(stopButtonName);
        newRandomGraphButton = new JButton("New random graph");
        stopButton.addActionListener(this);
        newRandomGraphButton.addActionListener(this);
        exportToTikzButton = new JButton("Export to TikZ");
        exportToTikzButton.addActionListener(this);
        randomColorsButton = new JButton("Random colors");
        randomColorsButton.addActionListener(this);
        layVerticesOnCircleButton = new JButton("Vertices on circle");
        layVerticesOnCircleButton.addActionListener(this);
        selectGraphFromFileButton = new JButton("Graph from file");
        selectGraphFromFileButton.addActionListener(this);

        edgeLengthSlider = new JSlider(20, 100);
        edgeLengthSlider.addChangeListener(changeEvent -> {
            fr.setK(edgeLengthSlider.getValue());
        });

        temperatureSlider = new JSlider(1, 10);
        temperatureSlider.addChangeListener(changeEvent -> {
            fr.setTemperature(temperatureSlider.getValue());
        });

        r.addMouseListener(this);
        r.addMouseMotionListener(this);
    }

    /**
     * Redraw the drawing panel.
     */
    public void update() {
        r.revalidate();
        r.repaint();
    }

    /**
     * Place window to the center of second display, if exists.
     * https://stackoverflow.com/questions/4627553/show-jframe-in-a-specific-screen-in-dual-monitor-configuration
     * @param   screen  (screen)-th screen
     * @param   frame   JFrame to place on the display
     */
    public static void showOnScreen(int screen, JFrame frame) {
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice[] gd = ge.getScreenDevices();
        if (screen > -1 && screen < gd.length) {
            // center the window on (screen)-th screen.
            Rectangle r = gd[screen].getDefaultConfiguration().getBounds();
            frame.setLocation( (int) r.getX() + frame.getWidth() / 2, (int)(r.getHeight() - frame.getHeight()) / 2);
        } else if (gd.length > 0) {
            frame.setLocation(gd[0].getDefaultConfiguration().getBounds().x, frame.getY());
        } else {
            throw new RuntimeException("No Screens Found");
        }
    }

    /**
     * Show GUI window.
     */
    public void showWindow() {
        JFrame f = new JFrame();

        r.addRenderable(ge);

        // create GUI
        JPanel rootPanel = new JPanel(new BorderLayout());
        rootPanel.add(r, BorderLayout.NORTH);

        JPanel toolPanel = new JPanel(new GridBagLayout());

        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;

        c.gridy = 0;
        c.gridx = 0;
        toolPanel.add(stopButton, c);
        c.gridx = 1;
        toolPanel.add(newRandomGraphButton, c);
        c.gridx = 2;
        toolPanel.add(layVerticesOnCircleButton, c);
        c.gridx = 3;
        toolPanel.add(randomColorsButton, c);

        c.gridx = 0;
        c.gridy = 1;
        toolPanel.add(new JLabel("Edge Length"), c);
        c.gridx = 1;
        toolPanel.add(edgeLengthSlider, c);
        c.gridx = 2;
        toolPanel.add(new JLabel("Temperature"), c);
        c.gridx = 3;
        toolPanel.add(temperatureSlider, c);

        c.gridx = 0;
        c.gridy = 2;
        c.gridwidth = 2;
        toolPanel.add(selectGraphFromFileButton, c);
        c.gridx = 2;
        toolPanel.add(exportToTikzButton, c);

        rootPanel.add(toolPanel, BorderLayout.SOUTH);

        f.add(rootPanel);

        // show window
        f.setTitle("Fruchterman-Reingolds");
        f.setResizable(false);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.pack();                       // will use preferred size (getPreferredSize method) of all the components
        f.setLocationRelativeTo(null);  // put window to center of screen
        showOnScreen(1, f);
        f.setVisible(true);

    }

    /**
     * Main procedure
     * @param   args    its arguments.
     */
    public static void main(String[] args) {
        Window w = new Window(new DrawableWindow(PANEL_WIDTH, PANEL_HEIGHT));
        w.showWindow();
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        if (actionEvent.getSource() == timer) {
            // call the algorithm
            fr.fruchtermanReingoldsIteration();

            update();
        }

        if (actionEvent.getSource() == stopButton) {
            if (stopButtonName.equals("Stop")) {
                stopButtonName = "Start";
                timer.stop();
            } else {
                stopButtonName = "Stop";
                timer.start();
            }

            stopButton.setText(stopButtonName);
        }

        if (actionEvent.getSource() == newRandomGraphButton) {
            timer.stop();
            try {
                int vertexCount = Integer.parseInt(JOptionPane.showInputDialog("Enter vertex count"));
                double edgeProbability = Double.parseDouble(JOptionPane.showInputDialog("Enter probability of an edge i.e. number between 0 and 1"));

                // remove previous graph
                r.removeRenderable(ge);

                // add new graph
                ge = GraphGallery.getRandomGraph(vertexCount, edgeProbability);
                fr = new FruchtermanReingolds(ge);
                r.addRenderable(ge);
                update();
            } catch (NumberFormatException e) {
                System.err.println("Invalid value was written.");
            }
        }

        if (actionEvent.getSource() == exportToTikzButton) {
            JFileChooser chooser = new JFileChooser();
            chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            int option = chooser.showOpenDialog(r);
            if (option == JFileChooser.APPROVE_OPTION) {
                File selectedFile = chooser.getSelectedFile();
                String path = selectedFile.getAbsolutePath();

                LatexTikZ l = new LatexTikZ(path, ge);

                try {
                    l.export();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        if (actionEvent.getSource() == selectGraphFromFileButton) {
            JFileChooser chooser = new JFileChooser();
            chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
            int option = chooser.showOpenDialog(r);
            if (option == JFileChooser.APPROVE_OPTION) {
                File selectedFile = chooser.getSelectedFile();
                String path = selectedFile.getAbsolutePath();

                if (!selectedFile.isFile()) {
                    try {
                        throw new Exception("This is not a file!");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                // remove previous graph
                r.removeRenderable(ge);

                DotFileReader dfr = new DotFileReader(path);
                ge = dfr.getGraph();

                // add new graph
                fr = new FruchtermanReingolds(ge);
                r.addRenderable(ge);
                update();
            }
        }

        if (actionEvent.getSource() == layVerticesOnCircleButton) {
            fr.layVerticesOnCircle();
            update();
        }

        if (actionEvent.getSource() == randomColorsButton) {
            ge.randomColors();
            update();
        }
    }

    // MouseMotionListener
    @Override
    public void mouseDragged(MouseEvent mouseEvent) {
        if (selectedVertex == null) return;
        selectedVertex.set(new Vector2D(mouseEvent.getX() - VertexEmbedding.RADIUS / 2d, mouseEvent.getY() - VertexEmbedding.RADIUS / 2d));
        update();
    }

    @Override
    public void mouseMoved(MouseEvent mouseEvent) {

    }



    // MouseListener
    @Override
    public void mouseClicked(MouseEvent mouseEvent) {

    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {
        for (VertexEmbedding ve : ge.vertices) {
            if (mouseEvent.getX() > ve.getX() && mouseEvent.getX() < ve.getX() + VertexEmbedding.RADIUS &&
                    mouseEvent.getY() > ve.getY() && mouseEvent.getY() < ve.getY() + VertexEmbedding.RADIUS
            ) {
                selectedVertex = ve;
            }
        }
    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {
        selectedVertex = null;
    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }
}
