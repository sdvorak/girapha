package window;

import draw.common.Drawable;
import draw.common.Renderable;

import java.awt.*;
import java.util.ArrayList;

/**
 * Specific window in which we can register <code>Drawable</code> components.
 */
public class DrawableWindow extends Renderable {

    private final ArrayList<Drawable> drawables;
    private final int width, height;

    /**
     * Constructor for setting dimensions of drawing JPanel.
     * @param   width   width of drawing JPanel
     * @param   height  height of drawing JPanel
     */
    public DrawableWindow(int width, int height) {
        this.drawables = new ArrayList<>();
        this.width = width;
        this.height = height;
        setPreferredSize(new Dimension(width, height));
    }

    @Override
    public void addRenderable(Drawable d) {
        this.drawables.add(d);
    }

    @Override
    public void removeRenderable(Drawable d) {
        drawables.remove(d);
    }

    @Override
    public void paintComponent(Graphics g) {
        draw(g);
    }

    @Override
    public void draw(Graphics g) {
        super.paintComponent(g);

        for (Drawable d : this.drawables) {
            d.draw(g);
        }
    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(width, height);
    }
}
