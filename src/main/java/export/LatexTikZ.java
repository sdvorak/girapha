package export;

import draw.graph.representation.EdgeEmbedding;
import draw.graph.representation.GraphEmbedding;
import draw.graph.representation.VertexEmbedding;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.DecimalFormat;

import static draw.DrawingConstants.PANEL_HEIGHT;
import static draw.DrawingConstants.PANEL_WIDTH;

public class LatexTikZ {

    private static final DecimalFormat df = new DecimalFormat("0.00");

    private final String header = "\\documentclass[tikz,border=5pt]{standalone}\n" +
            "\n" +
            "\\begin{document}\n" +
            "\n" +
            "\\begin{tikzpicture}\n";

    private final String footer = "\\end{tikzpicture}\n" +
            "\n" +
            "\\end{document}";

    private final String filename;
    private final GraphEmbedding ge;
    private final StringBuilder sb;

    public LatexTikZ(String filename, GraphEmbedding ge) {
        this.filename = filename;
        this.ge = ge;
        this.sb = new StringBuilder();
    }

    public void export() throws Exception {
        String verticesRepr;
        StringBuilder verticesReprBuilder = new StringBuilder();
        for (VertexEmbedding ve : ge.vertices) {
            verticesRepr = String.format("\\node[circle, fill={rgb,255:    red, %d;    green, %d;     blue, %d   },label=above:{%s}] (%s) at (%s, %s) {};\n",
                    ve.color.getRed(),
                    ve.color.getGreen(),
                    ve.color.getBlue(),
                    ve.name,
                    ve.name,
                    df.format(ve.getX() / PANEL_WIDTH * 15),
                    df.format(ve.getY() / PANEL_HEIGHT * 15)
            );

            verticesReprBuilder.append(verticesRepr);
        }

        sb.append(verticesReprBuilder);

        String edgesRepr;
        StringBuilder edgesReprBuilder = new StringBuilder();
        for (EdgeEmbedding ee : ge.edges) {
            edgesRepr = String.format("\\draw (%s) -- (%s);", ee.v1.name, ee.v2.name);
            edgesReprBuilder.append(edgesRepr);
        }

        sb.append(edgesReprBuilder);

        String fileContent = sb.toString();
        fileContent = header + fileContent;
        fileContent = fileContent + footer;

        // System.out.println(fileContent);

        // save to file
        File f = new File(filename);
        if (f.exists()) {
            throw new Exception("File already exists.");
        }

        if (!f.getParentFile().isDirectory()) {
            throw new Exception("Selected path is not a directory.");
        }

        try (PrintWriter out = new PrintWriter(filename)) {
            out.println(fileContent);
        } catch (FileNotFoundException e) {
            System.err.println("File not found.");
        }
    }


}
