package draw.graph.algorithm;

import draw.graph.representation.EdgeEmbedding;
import draw.graph.representation.GraphEmbedding;
import draw.graph.representation.VertexEmbedding;
import draw.util.Vector2D;
import window.Window;

import java.util.Random;

import static draw.DrawingConstants.*;

/**
 * Fruchterman-Reingolds algorithm
 * @see <a href="https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.13.8444&rep=rep1&type=pdf">Fruchterman-Reingolds paper</a>.
 */
public class FruchtermanReingolds {

    private final GraphEmbedding ge;

    private double temperature = 1.0;
    private final double C = 1;
    private double k;

    /**
     * Constructor.
     * @param   ge      Graph embedding
     */
    public FruchtermanReingolds(GraphEmbedding ge) {
        this.ge = ge;

        // preferred edge length
        this.k = Math.sqrt((double) PANEL_AREA / ge.vertexCount);

        //layVerticesOnCircle();
        randomizeInitPositions();
        centerGraph();
    }

    /**
     * Formula for calculating attractive force for edge-connected vertices.
     * @param   d   distance of two vectors
     * @return      attractive force
     */
    private double attractive(double d) {
        return Math.pow(d, 2) / k;
    }

    /**
     * Formula for calculating repulsive force for ALL vertices.
     * Notice that minus sign is omitted in comparison with the paper.
     * We add the minus sign afterwards.
     * @param       d   distance of two vectors
     * @return          repulsive force
     */
    private double repulsive(double d) {
        return Math.pow(k, 2) / Math.max(d, 0.01);
    }

    /**
     * Puts vertices on random positions.
     */
    public void randomizeInitPositions() {
        Random random = new Random();

        for (VertexEmbedding ve : ge.vertices) {
            ve.set(new Vector2D(
                    random.nextInt(PANEL_WIDTH),
                    random.nextInt(PANEL_HEIGHT)
            ));
        }
    }

    /**
     * Puts vertices on a circle (ellipse if screen is not square).
     */
    public void layVerticesOnCircle() {
        double angle = 2 * Math.PI / ge.vertexCount;

        for (int i = 0; i < ge.vertexCount; ++i) {

            final int xDisplacement = (int) (PANEL_WIDTH / 2.0);
            final int yDisplacement = (int) (PANEL_HEIGHT / 2.0);

            final int xRadius = (int) (.3 * PANEL_WIDTH);
            final int yRadius = (int) (.3 * PANEL_HEIGHT);

            ge.vertices.get(i).set(new Vector2D(
                    xDisplacement + xRadius * Math.cos(i * angle),
                    yDisplacement + yRadius * Math.sin(i * angle)
                    )
            );
        }
    }

    /**
     * Procedure for moving the graph to the center.
     */
    private void centerGraph() {
        double maxX = Integer.MIN_VALUE, maxY = Integer.MIN_VALUE, minX = Integer.MAX_VALUE, minY = Integer.MAX_VALUE;
        for (VertexEmbedding ve : ge.vertices) {
            maxX = Math.max(maxX, ve.getX());
            maxY = Math.max(maxY, ve.getY());

            minX = Math.min(minX, ve.getX());
            minY = Math.min(minY, ve.getY());
        }

        double graphWidth = maxX - minX;
        double graphHeight = maxY - minY;

        double targetX = (PANEL_WIDTH - graphWidth) / 2;
        double targetY = (PANEL_HEIGHT - graphHeight) / 2;

        for (VertexEmbedding ve : ge.vertices) {
            if (ve == Window.selectedVertex) continue;

            ve.set(new Vector2D(
                    (int) (ve.getX() + targetX - minX),
                    (int) (ve.getY() + targetY - minY)
                )
            );
        }
    }

    /**
     * Procedure for calculating repulsive force between
     * each pair of vertices.
     */
    public void calcRepulsive() {
        for (int i = 0; i < ge.vertexCount; ++i) {
            VertexEmbedding ve1 = ge.vertices.get(i);
            ve1.displacement = new Vector2D(0, 0);

            for (int j = 0; j < ge.vertexCount; ++j) {
                if (i == j) continue;

                VertexEmbedding ve2 = ge.vertices.get(j);

                Vector2D delta = ve1.newSubtract(ve2);
                double deltaLength = delta.magnitude();

                if (deltaLength <= 1) continue;

                Vector2D force = delta.newDivide(deltaLength)
                                .newMultiply(repulsive(deltaLength));
                ve1.displacement.add(force);
            }
        }
    }

    /**
     * Procedure for calculating attractive force between every
     * edge-connected pair of vertices.
     */
    public void calcAttractive() {
        for (EdgeEmbedding ee : ge.edges) {
            Vector2D delta = ee.v1.newSubtract(ee.v2);
            double deltaLength = delta.magnitude();

            if (deltaLength <= 1) continue;

            Vector2D force = delta.newDivide(deltaLength)
                            .newMultiply(attractive(deltaLength));
            ee.v1.displacement.subtract(force);
            ee.v2.displacement.add(force);
        }
    }

    /**
     * Finally, iteration of Fruchterman-Reingolds algorithm.
     */
    public void fruchtermanReingoldsIteration() {
        centerGraph();

        calcRepulsive();
        calcAttractive();

        for (VertexEmbedding ve : ge.vertices) {
            if (ve == Window.selectedVertex) continue;

            // displacement limited by temperature
            Vector2D epsilonDisplacement = new Vector2D(ve.displacement);
            double dist = epsilonDisplacement.magnitude();
            epsilonDisplacement.divide(dist);
            epsilonDisplacement.multiply(Math.min(dist, temperature));

            ve.add(epsilonDisplacement);

            // don't let the vertices go out of bounds of the window
            ve.set(
                    new Vector2D(
                            Math.min(PANEL_WIDTH - DRAWING_BORDER, Math.max(DRAWING_BORDER, ve.getX())),
                            Math.min(PANEL_HEIGHT - DRAWING_BORDER, Math.max(DRAWING_BORDER, ve.getY()))
                    )
            );

        }
    }

    /**
     * Setter of ideal edge length (for GUI slider purposes).
     * @param   k   ideal edge length
     */
    public void setK(double k) {
        this.k = k;
    }

    /**
     * Setter for temperature i.e. how fast the graph changes.
     * @param   temperature     temperature to set
     */
    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }
}
