package draw.graph.representation;

import draw.common.Drawable;

import java.awt.*;
import java.util.ArrayList;
import java.util.Random;

/**
 * Drawing the graph.
 */
public class GraphEmbedding implements Drawable {

    public final int vertexCount;
    public final int edgeCount;
    public final ArrayList<VertexEmbedding> vertices;
    public final ArrayList<EdgeEmbedding> edges;

    /**
     * Graph consists of list of vertices &amp; list of edges.
     * @param   vertices    list of vertices
     * @param   edges       list of edges
     */
    public GraphEmbedding(ArrayList<VertexEmbedding> vertices, ArrayList<EdgeEmbedding> edges) {
        this.vertices = vertices;
        this.edges = edges;
        this.vertexCount = vertices.size();
        this.edgeCount = edges.size();
    }

    /**
     * Assign each vertex random color.
     */
    public void randomColors() {
        Random random = new Random();
        for (VertexEmbedding ve : vertices) {
            ve.setColor(new Color(random.nextFloat(), random.nextFloat(), random.nextFloat()));
        }
    }

    /**
     * Find if two vertices are adjacent in O(|V|).
     * @param   v1  first vertex
     * @param   v2  second vertex
     * @return      true - if vertices are adjacent, false otherwise.
     */
    public boolean isAdjacent(VertexEmbedding v1, VertexEmbedding v2) {
        return edges.stream().anyMatch(ee -> (ee.v1 == v1 && ee.v2 == v2) || (ee.v1 == v2 && ee.v2 == v1));
    }

    @Override
    public void draw(Graphics g) {
        for (VertexEmbedding ve : vertices) {
            ve.draw(g);
        }

        for (EdgeEmbedding ee : edges) {
            ee.draw(g);
        }
    }
}
