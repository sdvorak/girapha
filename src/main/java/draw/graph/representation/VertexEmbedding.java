package draw.graph.representation;

import draw.common.Drawable;
import draw.util.Vector2D;

import java.awt.*;

/**
 * Representation of Vertex Embedding.
 */
public class VertexEmbedding extends Vector2D implements Drawable {

    // displacement
    public Vector2D displacement;
    public final String name;

    public Color color = Color.BLACK;

    public static int RADIUS = 20;

    /**
     * Constructor.
     * @param   position    position of vertex
     */
    public VertexEmbedding(Vector2D position) {
        super(position);
        this.displacement = new Vector2D(0, 0);
        this.name = "Unnamed";
    }

    /**
     * Constructor.
     * @param   position    position of vertex
     * @param   name        name of the vertex that will be rendered next to the vertex
     */
    public VertexEmbedding(Vector2D position, String name) {
        super(position);
        this.displacement = new Vector2D(0, 0);
        this.name = name;
    }

    /**
     * Constructor.
     * @param   name        name of the vertex that will be rendered next to the vertex
     */
    public VertexEmbedding(String name) {
        super(new Vector2D(0, 0));
        this.displacement = new Vector2D(0, 0);
        this.name = name;
    }

    @Override
    public void draw(Graphics g1) {
        // for circles with higher precision
        Graphics2D g = (Graphics2D) g1;
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        g.setColor(color);

        g.fillOval(
                (int) getX(),
                (int) getY(),
                RADIUS,
                RADIUS
        );

        g.drawString(
                name,
                (int) getX(),
                (int) getY()
        );

        // reset color to black
        g.setColor(Color.BLACK);
    }

    /**
     * Setter for color.
     * @param   color   color to assign to this vertex
     */
    public void setColor(Color color) {
        this.color = color;
    }
}
