package draw.graph.representation;

import draw.common.Drawable;
import draw.util.Vector2D;

import java.awt.*;

/**
 * Representation of Edge Embedding
 */
public class EdgeEmbedding implements Drawable {

    public final VertexEmbedding v1, v2;

    /**
     * Edge consists of two vertices.
     * @param   v1  first vertex
     * @param   v2  second vertex
     */
    public EdgeEmbedding(VertexEmbedding v1, VertexEmbedding v2) {
        this.v1 = v1;
        this.v2 = v2;
    }

    @Override
    public void draw(Graphics g1) {
        Graphics2D g = (Graphics2D) g1;
        g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        Vector2D displacement = new Vector2D((double)VertexEmbedding.RADIUS / 2, (double)VertexEmbedding.RADIUS / 2);

        Vector2D start = new Vector2D(v1);
        start.add(displacement);
        Vector2D end = new Vector2D(v2);
        end.add(displacement);

        g.drawLine(
                (int)(start.getX()),
                (int)(start.getY()),
                (int)(end.getX()),
                (int)(end.getY())
        );
    }
}
