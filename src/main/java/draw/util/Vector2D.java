package draw.util;

/**
 * Vector calculations in 2D.
 */
public class Vector2D {

    private double x, y;

    public Vector2D(double x, double y) {
        this.x = x;
        this.y = y;
    }

    public Vector2D() {
        this.x = 0;
        this.y = 0;
    }

    // copy constructor
    public Vector2D(Vector2D v) {
        this.x = v.x;
        this.y = v.y;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    /**
     * Euclidean distance between this Vector2D and the 'other' Vector2D.
     * @param other Other vector
     * @return distance
     */
    public double distance(Vector2D other) {
        double dx = this.x - other.x;
        double dy = this.y - other.y;
        // pow will make it positive, so we don't need absolute value
        return Math.sqrt(Math.pow(dx, 2) + Math.pow(dy, 2));
    }

    /**
     * Vector length.
     * Something like euclidean distance but on an already created vector.
     * @return
     */
    public double magnitude() {
        return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2));
    }

    /**
     * Divide vector by constant.
     * @param divisor vector divisor
     */
    public void divide(double divisor) {
        this.x /= divisor;
        this.y /= divisor;
    }

    public Vector2D newDivide(double divisor) {
        Vector2D cpy = new Vector2D(this);
        cpy.x /= divisor;
        cpy.y /= divisor;

        return cpy;
    }

    /**
     * Multiply vector by constant.
     * @param multiplier vector multiplier
     */
    public void multiply(double multiplier) {
        this.x *= multiplier;
        this.y *= multiplier;
    }

    public Vector2D newMultiply(double multiplier) {
        Vector2D cpy = new Vector2D(this);
        cpy.x *= multiplier;
        cpy.y *= multiplier;

        return cpy;
    }

    public void multiply(Vector2D multiplier) {
        this.x *= multiplier.getX();
        this.y *= multiplier.getY();
    }

    /**
     * Add constant to vector.
     * @param addition constant to add
     */
    public void add(double addition) {
        this.x += addition;
        this.y += addition;
    }

    public void add(Vector2D addition) {
        this.x += addition.getX();
        this.y += addition.getY();
    }

    /**
     * Subtract constant from vector.
     * @param subtraction constant to subtract
     */
    public void subtract(double subtraction) {
        this.x -= subtraction;
        this.y -= subtraction;
    }

    public void subtract(Vector2D subtraction) {
        this.x -= subtraction.getX();
        this.y -= subtraction.getY();
    }

    public Vector2D newSubtract(Vector2D subtraction) {
        Vector2D cpy = new Vector2D(this);

        cpy.x -= subtraction.getX();
        cpy.y -= subtraction.getY();
        return cpy;
    }

    public Vector2D min(Vector2D bound) {
        return new Vector2D(Math.min(x, bound.getX()), Math.min(y, bound.getY()));
    }

    public void set(Vector2D which) {
        this.x = which.getX();
        this.y = which.getY();
    }

    @Override
    public String toString() {
        return "(" + getX() + ", " + getY() + ")";
    }
}
