package draw.util;

import draw.graph.representation.EdgeEmbedding;
import draw.graph.representation.GraphEmbedding;
import draw.graph.representation.VertexEmbedding;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Reader of files with subset of DOT format <a href="https://en.wikipedia.org/wiki/DOT_(graph_description_language)">DOT Format (Wikipedia)</a>.
 */
public class DotFileReader {

    private final String filename;
    private final ArrayList<VertexEmbedding> ave = new ArrayList<>();
    private final ArrayList<EdgeEmbedding> aee = new ArrayList<>();
    private enum PATTERN_TYPE {VERTEX_TO_VERTEX, VERTEX_TO_VERTICES};
    private final HashMap<PATTERN_TYPE, Pattern> patterns;
    private final HashMap<String, VertexEmbedding> nameToVertex;

    /**
     * Constructor.
     * @param   filename  file to read and convert to GraphEmbedding instance.
     */
    public DotFileReader(String filename) {
        this.filename = filename;

        this.nameToVertex = new HashMap<>();

        this.patterns = new HashMap<>();
        this.patterns.put(PATTERN_TYPE.VERTEX_TO_VERTEX,
                Pattern.compile("(?<firstVertexName>[a-zA-Z0-9]+)\\s*((->)|(--))\\s*(?<secondVertexName>[a-zA-Z0-9]+);?")
        );

        this.patterns.put(PATTERN_TYPE.VERTEX_TO_VERTICES,
                Pattern.compile("(?<firstVertexName>[a-zA-Z0-9]+)\\s*((->)|(--))\\s*\\{\\s*(?<targetVertices>([a-zA-Z0-9]\\s+)+)\\s*\\};?")
        );
    }

    /**
     * Convert given file to GraphEmbedding instance.
     * @return  GraphEmbedding instance representing the DOT file.
     */
    public GraphEmbedding getGraph() {
        try (BufferedReader br = new BufferedReader(new FileReader(filename))) {
            String line;
            while ((line = br.readLine()) != null) {
                if (line.trim().isEmpty() || line.startsWith("#")) continue;

                parseDotLine(line);
            }
        } catch (FileNotFoundException e) {
            System.err.println("File " + filename + " was not found.");
        } catch (IOException e) {
            System.err.println("Problem occurred.");
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new GraphEmbedding(ave, aee);
    }

    /**
     * Lines in DOT file are of different types. This method finds out.
     * @param               line    line to be parsed
     * @throws Exception            exception if thrown
     */
    private void parseDotLine(String line) throws Exception {
        for (PATTERN_TYPE pt : patterns.keySet()) {
            Pattern p = patterns.get(pt);
            Matcher m = p.matcher(line);

            if (!m.matches()) continue;

            switch (pt) {
                case VERTEX_TO_VERTEX: {
                    String name1 = m.group("firstVertexName");
                    String name2 = m.group("secondVertexName");

                    handleEdge(name1, name2);

                    return;
                }
                case VERTEX_TO_VERTICES: {
                    String name1 = m.group("firstVertexName");
                    String names = m.group("targetVertices");

                    names = names.trim();

                    for (String name2 : names.split("\\s+")) {
                        handleEdge(name1, name2);
                    }

                    return;
                }
            }
        }

        throw new Exception("Line: \"" + line + "\" is not according to any of the supported formats.");
    }

    /**
     * Add edge.
     * @param   name1   name of a starting vertex of the edge
     * @param   name2   name of an ending vertex of the edge
     */
    private void handleEdge(String name1, String name2) {
        VertexEmbedding ve1 = new VertexEmbedding(name1);
        VertexEmbedding ve2 = new VertexEmbedding(name2);

        if (!nameToVertex.containsKey(name1)) {
            nameToVertex.put(name1, ve1);
            ave.add(ve1);
        } else {
            ve1 = nameToVertex.get(name1);
        }

        if (!nameToVertex.containsKey(name2)) {
            nameToVertex.put(name2, ve2);
            ave.add(ve2);
        } else {
            ve2 = nameToVertex.get(name2);
        }

        aee.add(new EdgeEmbedding(ve1, ve2));
    }

}
