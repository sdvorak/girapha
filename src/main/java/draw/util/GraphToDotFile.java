package draw.util;

import draw.graph.representation.EdgeEmbedding;
import draw.graph.representation.GraphEmbedding;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

public class GraphToDotFile {

    private final GraphEmbedding ge;

    public GraphToDotFile(GraphEmbedding ge) {
        this.ge = ge;
    }

    public void createDotFile(String filename) throws Exception {
        File f = new File(filename);
        if (f.exists()) {
            throw new Exception("File already exists.");
        }

        if (!f.getParentFile().isDirectory()) {
            throw new Exception("Selected path is not a directory.");
        }

        StringBuilder sb = new StringBuilder();

        for (EdgeEmbedding ee : ge.edges) {
            sb.append(ee.v1.name);
            sb.append("->");
            sb.append(ee.v2.name);
            sb.append("\n");
        }

        String fileContent = sb.toString();

        try (PrintWriter out = new PrintWriter(filename)) {
            out.println(fileContent);
        } catch (FileNotFoundException e) {
            System.err.println("File not found.");
        }
    }

}
