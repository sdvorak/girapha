package draw.util;

import draw.graph.representation.EdgeEmbedding;
import draw.graph.representation.GraphEmbedding;
import draw.graph.representation.VertexEmbedding;

import java.util.ArrayList;
import java.util.Random;

/**
 * Gallery of useful / special / interesting graphs.
 */
public class GraphGallery {

    /**
     * Generates complete graph <a href="https://en.wikipedia.org/wiki/Complete_graph">Complete graph</a> on <code>N</code> vertices.
     * @param       N   how many vertices we want
     * @return          GraphEmbedding instance of graph K_N (complete graph on <code>N</code> vertices)
     */
    public static GraphEmbedding getCompleteGraph(int N) {
        ArrayList<VertexEmbedding> ave = new ArrayList<>();
        for (int i = 1; i <= N; ++i) {
            VertexEmbedding ve = new VertexEmbedding("ve" + i);
            ave.add(ve);
        }

        ArrayList<EdgeEmbedding> aee = new ArrayList<>();
        for (int i = 1; i <= N; ++i) {
            for (int j = i + 1; j <= N; ++j) {
                EdgeEmbedding ee = new EdgeEmbedding(ave.get(i - 1), ave.get(j - 1));
                aee.add(ee);
            }
        }

        return new GraphEmbedding(ave, aee);
    }

    /**
     * Random graph.
     * @param vertexCount           count of vertices we want
     * @param probabilityOfEdge     probability of an edge between any two vertices
     * @return                      GraphEmbedding of specified graph
     */
    public static GraphEmbedding getRandomGraph(int vertexCount, double probabilityOfEdge) {
        ArrayList<VertexEmbedding> ave = new ArrayList<>();
        ArrayList<EdgeEmbedding> aee = new ArrayList<>();

        for (int i = 0; i < vertexCount; ++i) {
            ave.add(new VertexEmbedding("ve" + i));
        }

        Random random = new Random();
        for (int i = 0; i < vertexCount; ++i) {
            for (int j = i+1; j < vertexCount; ++j) {

                // returns double between "0.0" and "1.0"
                if (random.nextDouble() < probabilityOfEdge) {
                    // make edge between vertex 'i' and 'j'
                    aee.add(new EdgeEmbedding(ave.get(i), ave.get(j)));
                }
            }
        }

        return new GraphEmbedding(ave, aee);
    }

    /**
     * Cubus.
     * @param   cubusCount  how many edges should the cubus have
     * @return              GraphEmbedding of the cubus
     */
    public static GraphEmbedding getCubus(int cubusCount) {
        ++cubusCount;
        ArrayList<VertexEmbedding> ave = new ArrayList<>();
        ArrayList<EdgeEmbedding> aee = new ArrayList<>();

        VertexEmbedding[][][] vertexNames = new VertexEmbedding[cubusCount][cubusCount][cubusCount];

        // create vertices
        for (int x = 0; x < cubusCount; ++x) {
            for (int y = 0; y < cubusCount; ++y) {
                for (int z = 0; z < cubusCount; ++z) {
                    String name = String.valueOf(x) + y + z;
                    VertexEmbedding ve = new VertexEmbedding(name);
                    vertexNames[x][y][z] = ve;
                    ave.add(ve);
                }
            }
        }

        int[] moves = {0, 1};

        // create edges
        for (int x = 0; x < cubusCount; ++x) {
            for (int y = 0; y < cubusCount; ++y) {
                for (int z = 0; z < cubusCount; ++z) {

                    for (int moveX : moves) {
                        if (x + moveX >= cubusCount) continue;
                        aee.add(new EdgeEmbedding(vertexNames[x][y][z], vertexNames[x + moveX][y][z]));
                    }

                    for (int moveY : moves) {
                        if (y + moveY >= cubusCount) continue;
                        aee.add(new EdgeEmbedding(vertexNames[x][y][z], vertexNames[x][y + moveY][z]));
                    }

                    for (int moveZ : moves) {
                        if (z + moveZ >= cubusCount) continue;
                        aee.add(new EdgeEmbedding(vertexNames[x][y][z], vertexNames[x][y][z + moveZ]));
                    }
                }
            }
        }

        return new GraphEmbedding(ave, aee);
    }
}
