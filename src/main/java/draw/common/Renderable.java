package draw.common;

import javax.swing.*;

/**
 * JPanel that has <code>Drawable</code> components that are drawn.
 */
public abstract class Renderable extends JPanel implements Drawable {
    public abstract void addRenderable(Drawable d);
    public abstract void removeRenderable(Drawable d);
}
