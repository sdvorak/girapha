package draw.common;

import java.awt.*;

/**
 * Classes implementing this interface do have some graphical representation, that can be drawn.
 */
public interface Drawable {
    void draw(Graphics g);
}
