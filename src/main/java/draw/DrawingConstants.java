package draw;

public class DrawingConstants {
    /**
     * Drawing JPanel width and height - NOT THE WHOLE WINDOW!
     */
    public static final int PANEL_WIDTH = 1000, PANEL_HEIGHT = 800;
    public static final int PANEL_AREA = PANEL_WIDTH * PANEL_HEIGHT;
    public static final int DRAWING_BORDER = 20;

}
