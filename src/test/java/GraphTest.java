import draw.graph.representation.GraphEmbedding;
import draw.util.GraphGallery;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class GraphTest {

    @Test
    public void testGraph() throws Exception {
        GraphEmbedding ge = GraphGallery.getCompleteGraph(5);

        assertTrue(ge.isAdjacent(ge.vertices.get(0), ge.vertices.get(1)));
        assertTrue(ge.isAdjacent(ge.vertices.get(0), ge.vertices.get(2)));
        assertTrue(ge.isAdjacent(ge.vertices.get(0), ge.vertices.get(3)));
    }

}